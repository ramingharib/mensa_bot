import unittest
from collections import defaultdict
from typing import Optional

from models.ingredients import Ingredients


def _nth_ind_true(n: Optional[int] = None) -> tuple[bool, ...]:
    default = [False, False, False, False, False, False]
    if n is not None:
        default[n] = True
    return tuple(default)


class IngredientsTest(unittest.TestCase):
    def test_formatting(self):
        ing = Ingredients(defaultdict(lambda: False), "")
        self.assertEqual(str(ing), "")

        ing.chicken = True
        self.assertEqual(str(ing), " 🐔")

        ing.fish = True
        self.assertEqual(str(ing), " 🐟")

        ing.vegetarian = True
        self.assertEqual(str(ing), " 🌽")

        ing.vegetarian = False
        ing.vegan = True
        self.assertEqual(str(ing), " 🥦")

    def test_meat_indicators_title_is_none(self):
        ing = Ingredients(defaultdict(lambda: False), None)
        self.assertEqual(ing._check_meat_indicators(None), _nth_ind_true())

    def test_meat_indicators(self):
        ing = Ingredients(defaultdict(lambda: False), "")
        indicators = ing._check_meat_indicators(
            "Gekochte Eier in Senfsoße dazu Salzkartoffeln"
        )
        self.assertEqual(indicators, _nth_ind_true())

        indicators = ing._check_meat_indicators(
            "Rindergulasch mit Pilzen dazu Gemüse und Salzkartoffeln"
        )
        self.assertEqual(indicators, _nth_ind_true(1))

        indicators = ing._check_meat_indicators(
            "Hähnchen Cordon Bleu mit Gemüse, dazu Kroketten oder Eierknöpfle"
        )
        self.assertEqual(indicators, _nth_ind_true(2))

        indicators = ing._check_meat_indicators("Veggie-Burger mit Süßkartoffel-Pommes")
        self.assertEqual(indicators, _nth_ind_true(5))
