import json
import os.path
import unittest

from griebnitzsee_mensa.ulfparser import UlfParser


class UlfParserTest(unittest.TestCase):
    def test_parser(self):
        file = "tests/ulf.json" if os.path.isfile("tests/ulf.json") else "ulf.json"
        with open(file, "r", encoding="utf-8") as offer_example_file:
            ULF = json.loads(offer_example_file.read())

        u = UlfParser()
        result = u._parse_json(ULF)
        self.assertEqual(len(result), 4)

        result = [str(o) for o in result]
        self.assertEqual(
            result,
            [
                "\n🍽️ Ulf's Tagesessen 1 🐑\nLammcurry mit Reis und Salat\n💰 6,00€\n",
                "\n🍽️ Ulf's Tagesessen 2\nSüsskartoffeln mit Brie überbacken\n💰 6,50€\n",
                "\n🍽️ Ulf's Tagesessen 3\nBelugalinsen mit Hirtenkäse und Salat\n💰 6,00€\n",
                "\n🍽️ Ulf's Tagesessen 4\nNudel-Gemüseauflauf mit Salat\n💰 6,00€\n",
            ],
        )

    def test_download(self):
        self.assertIsNotNone(UlfParser()._download_json("2021-07-29"))
