import json
import unittest
from typing import Any, Dict

from griebnitzsee_mensa.mensaparser import MensaParser

with open("tests/offers.json", "r", encoding="utf-8") as offer_example_file:
    OFFERS = offer_example_file.read()
    OFFERS = json.loads(OFFERS)


class MensaParserTest(unittest.TestCase):
    def test_get_json_from_uri(self):
        res = MensaParser()._download_json()
        self.assertEqual(res["status_code"], 200)

    def test_is_work_day(self):
        def to_data_dict(t: Any) -> Dict[str, Any]:
            return {"wochentag": t}

        self.assertTrue(MensaParser()._is_workday(to_data_dict(1)))
        self.assertTrue(MensaParser()._is_workday(to_data_dict("1")))
        self.assertFalse(MensaParser()._is_workday(to_data_dict(0)))
        self.assertFalse(MensaParser()._is_workday(to_data_dict("0")))
        self.assertFalse(MensaParser()._is_workday(to_data_dict(7)))
        self.assertFalse(MensaParser()._is_workday(to_data_dict("7")))

    def test_is_holiday(self):
        data = {}
        self.assertTrue(MensaParser()._is_holiday(data))

    def test_is_not_holiday(self):
        data = {"angebote": {"fmwert": 1}}
        self.assertFalse(MensaParser()._is_holiday(data))

    def test_should_send_data(self):
        data = {"angebote": {"fmwert": 1}, "wochentag": 1}
        p = MensaParser()
        p._data = data
        self.assertTrue(p.should_send_data())

    def test_parse_offers(self):
        data = [{"titel": "Angebot 1", "beschreibung": "broooot"}]
        self.assertEqual(1, len(MensaParser()._parse_offers(data)))

    def test_fill_offers(self):
        p = MensaParser()
        p._data = OFFERS

        offers_list = p.get_todays_offers()
        self.assertEqual(len(offers_list), 4)

    def test_parser_execution(self):
        p = MensaParser()
        self.assertIsInstance(p.get_todays_offers(), list)

    def test_str(self):
        p = MensaParser()
        p._data = OFFERS
        result = [str(o) for o in p.get_todays_offers()]
        self.assertEqual(
            str(result),
            "['\\n🍽️ Angebot 1 🐖\\nSoljanka mit Sauerrahm und Vollkornbrötchen\\n💰 1,60€ | 3,10€\\n', '\\n🍽️ Angebot 3 "
            "🐖\\nSchweineschnitzel mit Schwarzwurzel-Karottengemüse, dazu Kroketten oder Salzkartoffeln\\n💰 2,"
            "60€ | 4,50€\\n', '\\n🍽️ Angebot 4 🥦\\nTortelloni-Gemüse-Pfanne mit Tomatensugo\\n💰 2,60€ | 4,50€\\n', "
            "'\\n🍽️ Tagessuppe 🌽\\nGemüserahmsuppe\\n💰 1,00€\\n']",
        )
