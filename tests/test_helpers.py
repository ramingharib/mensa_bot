import json
import os.path
import unittest
from datetime import datetime

from griebnitzsee_mensa.helpers import save_sent_message, get_last_sent_message


class IngredientsTest(unittest.TestCase):
    refectory = "(Ulf) "

    def test_write_last_sent(self):
        file_name = save_sent_message(self.refectory, "test message. Hallo 👋")
        self.assertTrue(os.path.isfile(file_name))
        with open(file_name, "r") as f:
            self.assertEqual(
                json.load(f),
                {
                    "sent_on": datetime.today().strftime("%Y-%m-%d"),
                    "message": "test message. Hallo 👋",
                },
            )

    def test_read_last_sent_none_available(self):
        if os.path.isfile(f"/tmp/{self.refectory}.json"):
            os.remove(f"/tmp/{self.refectory}.json")

        self.assertIsNone(get_last_sent_message(self.refectory))

    def test_read_last_sent_earlier_available(self):
        if os.path.isfile(f"/tmp/{self.refectory}.json"):
            os.remove(f"/tmp/{self.refectory}.json")
        save_sent_message(self.refectory, "test message. Hallo 👋")
        self.assertEqual(get_last_sent_message(self.refectory), "test message. Hallo 👋")

    def test_read_last_sent_yesterday_available(self):
        if os.path.isfile(f"/tmp/{self.refectory}.json"):
            os.remove(f"/tmp/{self.refectory}.json")

        with open(f"/tmp/{self.refectory}.json", "w") as f:
            msg = {"sent_on": "2019-12-12", "message": "test message. Hallo 👋"}
            json.dump(msg, f)

        self.assertIsNone(get_last_sent_message(self.refectory))
