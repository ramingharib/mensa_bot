import os
import unittest
from typing import List

from griebnitzsee_mensa.manager import Manager, Error
from models.offer import Offer


def _clean_message_store():
    if os.path.isfile("/tmp/(Mensa) .json"):
        os.remove("/tmp/(Mensa) .json")
    if os.path.isfile("/tmp/(Ulf) .json"):
        os.remove("/tmp/(Ulf) .json")
    if os.path.isfile("/tmp/.json"):
        os.remove("/tmp/.json")


class ManagerTest(unittest.TestCase):
    # make type checker errors go away with List[Offer]
    error_message = "Expected to send a/n {} message. Is it holiday/sunday? Got {}"
    ulf: List[Offer] = ["ulf"]
    mensa: List[Offer] = ["mensa", "mensa"]
    ck1: List[Offer] = ["ck1", "ck1", "ck1"]

    def setUp(self):
        _clean_message_store()

    def test_message_content_both(self):
        m = Manager(True, True)
        offers, name = m._get_message_content(self.ulf, self.mensa, self.ck1)
        self.assertEqual(len(offers), 6)
        self.assertEqual(name, "")

    def test_message_content_ulf(self):
        m = Manager(send_ulf=True, send_mensa=False, send_ck1=False)
        offers, name = m._get_message_content(self.ulf, self.mensa, self.ck1)
        self.assertEqual(len(offers), 1)
        self.assertEqual(name, "(Ulf) ")

    def test_message_content_mensa(self):
        m = Manager(send_ulf=False, send_mensa=True, send_ck1=False)
        offers, name = m._get_message_content(self.ulf, self.mensa, self.ck1)
        self.assertEqual(len(offers), 2)
        self.assertEqual(name, "(Mensa) ")

    def test_message_content_ck1(self):
        m = Manager(send_ulf=False, send_mensa=False, send_ck1=True)
        offers, name = m._get_message_content(self.ulf, self.mensa, self.ck1)
        self.assertEqual(len(offers), 3)
        self.assertEqual(name, "(Campus Kitchen One) ")

    def test_format_message(self):
        m = Manager()
        r = m._format_message("test ", [])
        self.assertTrue(r.startswith("🍴 Speiseplan test für "))
        self.assertEqual(len(r.splitlines()), 1)

        r = m._format_message("test ", self.ulf)
        self.assertTrue(r.startswith("🍴 Speiseplan test für "))
        self.assertEqual(len(r.splitlines()), 2)

    def test_send_once_if_same_message(self):
        manager = Manager(send_ulf=False)
        _clean_message_store()

        execution_result = manager.execute()
        self.assertIsNone(execution_result)

        execution_result = manager.execute()
        self.assertEqual(execution_result, Error.SAME_MESSAGE_CONTENT)

    def test_manager_execute_ulf(self):
        manager = Manager(send_mensa=False, send_ck1=False)
        execution_result = manager.execute()
        self.assertIsNone(
            execution_result, self.error_message.format("ulf", execution_result)
        )

    def test_manager_execute_mensa(self):
        manager = Manager(send_ulf=False, send_ck1=False)
        execution_result = manager.execute()
        self.assertIsNone(
            execution_result, self.error_message.format("mensa", execution_result)
        )

    def test_manager_execute_ck1(self):
        manager = Manager(send_ulf=False, send_mensa=False)
        execution_result = manager.execute()
        self.assertIsNone(
            execution_result, self.error_message.format("ck1", execution_result)
        )

    def test_manager_execute_both(self):
        manager = Manager()
        execution_result = manager.execute()
        self.assertIsNone(
            execution_result, self.error_message.format("all", execution_result)
        )

    @staticmethod
    def test_format_message_printout():
        m = Manager()
        ulf = m._ulf_parser.get_todays_offers()
        mensa = m._mensa_parser.get_todays_offers()
        ck1 = m._ck1_parser.get_todays_offers()
        offers, refectory_name = m._get_message_content(ulf, mensa, ck1)

        print("--------------------- BEGIN MESSAGE TEST PRINTOUT ---------------------")
        print(m._format_message(refectory_name, offers))
        print("---------------------- END MESSAGE TEST PRINTOUT ----------------------")

    def test_send_telegram_message(self):
        self.assertTrue(Manager()._send_telegram_message("test message. Hallo 👋"))
