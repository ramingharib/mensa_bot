from griebnitzsee_mensa.openmensaparser import OpenmensaParser

CANTEEN_ID = 112
MEAL_PREFIX = "Ulf's"


class UlfParser(OpenmensaParser):
    def __init__(self):
        super().__init__(CANTEEN_ID, MEAL_PREFIX)
