import json
from collections import defaultdict
from datetime import datetime
from json import JSONDecodeError
from typing import List, Dict, Optional

import requests

from models.offer import Offer


class OpenmensaParser:
    def __init__(self, canteen_id: int, meal_prefix: Optional[str] = None):
        self._canteen_id = canteen_id
        self._openmensa_base_url = (
            "https://openmensa.org/api/v2/canteens/{canteen_id}/days/{date}/meals"
        )
        self._meal_prefix = meal_prefix

    def _download_json(self, date: str) -> List[Dict[str, any]]:
        url = self._openmensa_base_url.format(canteen_id=self._canteen_id, date=date)
        request_response = requests.get(url)

        if request_response.status_code == 404:
            return []

        try:
            result = json.loads(request_response.content)
            return result
        except JSONDecodeError as e:
            print(request_response.content)
            raise e

    def _parse_json(self, json_data: List[Dict[str, any]]) -> List[Offer]:
        result = []
        for meal_object in json_data:
            if self._meal_prefix is not None:
                title = "{} {}".format(self._meal_prefix, meal_object["category"])
            else:
                title = meal_object["category"]
            ingredients = defaultdict(lambda: False)
            for note in meal_object["notes"]:
                note = note.lower()
                if "vegetari" in note:
                    ingredients["vegetarisch"] = True
                if "schwein" in note:
                    ingredients["sau"] = True
                for ing in [
                    "hahn",
                    "rind",
                    "fisch",
                    "alkohol",
                    "knoblauch",
                    "lamm",
                    "vegan",
                ]:
                    if ing in note:
                        ingredients[ing] = True
            price_students = meal_object.get("prices").get("students", None)
            price_others = meal_object.get("prices").get("others", -1.0)
            if price_students is None and price_others is not None:
                price_students = price_others
            result.append(
                Offer(
                    title,
                    meal_object["name"],
                    price_students,
                    price_others,
                    ingredients,
                )
            )

        return result

    def get_todays_offers(self) -> List[Offer]:
        json_data = self._download_json(datetime.today().strftime("%Y-%m-%d"))
        return self._parse_json(json_data)
