This program scrapes the [Mensa Griebnitzsee](https://www.studentenwerk-potsdam.de/essen/unsere-mensen-cafeterien/detailinfos/?tx_ddfmensa_ddfmensa%5Bmensa%5D=6&tx_ddfmensa_ddfmensa%5Baction%5D=show&cHash=42918cc76cdcb7a31c8fb2187b846933) website and sends the food offers to the
[Mensa Griebnitzsee channel in Telegram](https://t.me/mensa_griebnitzsee) everyday, except Sunday, at 11 am.

It needs the environment variable `TELEGRAM_BOT_API_KEY` to be set to run correctly.