FROM alpine
MAINTAINER enra64 <>

# python setup
RUN apk add --update --no-cache python3 tzdata && rm  -rf /tmp/* /var/cache/apk/*
ENV TZ=Europe/Berlin
ENV PYTHONUNBUFFERED=1
RUN ln -sf python3
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

WORKDIR /opt/mensa_bot
ENV PYTHONPATH=.
ADD requirements.txt .
RUN pip install -r requirements.txt

COPY crontab /var/spool/cron/crontabs/root
ADD . .

CMD crond -f -S -l 0
