from typing import Dict, Any, Optional, Tuple


class Ingredients:
    pig_indicators = ["schwein", "pork", "carbonara"]
    beef_indicators = ["rind"]
    chicken_indicators = [
        "hühnchen",
        "hähnchen",
        "pute",
        "chicken",
        "geflügel",
        "hühner",
        "huhn",
    ]
    lamb_indicators = ["lamm"]
    fish_indicators = ["fisch", "karpfe", "lachs"]
    burger_indicators = ["burger"]

    def __init__(self, ingredients_dic: Dict[str, Any], dish_title: Optional[str]):
        self.chicken = ingredients_dic["hahn"]
        self.beef = ingredients_dic["rind"]
        self.pig = ingredients_dic["sau"]
        self.fish = ingredients_dic["fisch"]
        self.alcohol = ingredients_dic["alkohol"]
        self.garlic = ingredients_dic["knoblauch"]
        self.lamb = ingredients_dic["lamm"]
        self.vegetarian = ingredients_dic["vegetarisch"]
        self.vegan = ingredients_dic["vegan"]
        self.vital = ingredients_dic["vital"]
        self.flexitarisch = ingredients_dic["flexitarisch"]
        self.burger = False
        self.dish_title = dish_title

    def _apply_indicator_logic(self):
        # I assume that default meat plus pig means that someone selected "contains pig"
        # A dish with an indicator in the title might be corrected in the next section
        # For now, disable the default meats: chicken and beef
        default_meat_plus_pig = self.chicken and self.beef and self.pig
        if default_meat_plus_pig:
            self.beef = False
            self.chicken = False

        pig, beef, chicken, lamb, fish, burger = self._check_meat_indicators(
            self.dish_title
        )
        found_indicator = pig or beef or chicken or lamb or fish or burger
        is_vegx = self.vegetarian or self.vegan

        # if marked as vegetarian, (probably) set all to false
        if is_vegx or found_indicator:
            self.pig = pig
            self.beef = beef
            self.chicken = chicken
            self.lamb = lamb
            self.fish = fish
            self.burger = burger
        elif self.fish:
            self.pig, self.beef, self.chicken, self.lamb = False, False, False, False

    def _check_meat_indicators(
        self, title: Optional[str]
    ) -> Tuple[bool, bool, bool, bool, bool, bool]:
        """
        tuple: pig, beef, chicken, lamb, fish, burger
        """
        if title is None:
            return False, False, False, False, False, False

        title = title.lower()
        pig = any(indicator in title for indicator in self.pig_indicators)
        beef = any(indicator in title for indicator in self.beef_indicators)
        chicken = any(indicator in title for indicator in self.chicken_indicators)
        lamb = any(indicator in title for indicator in self.lamb_indicators)
        fish = any(indicator in title for indicator in self.fish_indicators)
        burger = any(indicator in title for indicator in self.burger_indicators)

        return pig, beef, chicken, lamb, fish, burger

    def __str__(self):
        emoji_list = []
        self._apply_indicator_logic()

        if self.chicken:
            emoji_list.append("🐔")
        if self.beef:
            emoji_list.append("🐄")
        if self.pig:
            emoji_list.append("🐖")
        if self.fish:
            emoji_list.append("🐟")
        if self.burger:
            emoji_list.append("🍔")
        if self.alcohol:
            emoji_list.append("🥃")
        if self.lamb:
            emoji_list.append("🐑")
        if self.vegetarian:
            emoji_list.append("🌽")
        if self.vegan:
            emoji_list.append("🥦")
        if self.garlic:
            emoji_list.append("🧄")

        if len(emoji_list) == 0:
            return ""

        return " " + ("{}" * len(emoji_list)).format(*emoji_list)
